<?php

namespace App\Interfaces;

interface CharacterInterface
{
    /**
     * get skills of character
     * @return string
     */
    public function getSkill();
}