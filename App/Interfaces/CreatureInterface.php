<?php

namespace App\Interfaces;

interface CreatureInterface
{
    /**
     * get creature's voice
     * @return  string
     */
    public function getVoice();
}