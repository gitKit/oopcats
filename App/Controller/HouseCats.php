<?php

namespace App\Controller;

use App\Animals\Cats\HouseCat;
use App\Service\Database;

class HouseCats
{
    public function execute()
    {
        $arr = Database::houseCatsData();

        echo "
            <h3>My house cats</h3>
            <table style='border: 1px solid black;'>
            <tr>
                <th style='border: 1px solid black;'>id</th>
                <th style='border: 1px solid black;'>name</th>
                <th style='border: 1px solid black;'>age</th>
                <th style='border: 1px solid black;'>breed</th>
                <th style='border: 1px solid black;'>skin color</th>
                <th style='border: 1px solid black;'>temperament</th>
            </tr>
        ";

        for ($i = 0; $i < count($arr); $i++)
        {
            $cat = new HouseCat($arr[$i]["name"],
                                $arr[$i]["age"],
                                $arr[$i]["breed"],
                                $arr[$i]["skin_color"],
                                $arr[$i]["temperament"]);
            $catInfo = $cat->getCatInfo();

            echo "<tr>
                      <td style='border: 1px solid black;'>"
                      . $arr[$i]["id"] .
                      "</td>";
            foreach ($catInfo as $param)
            {
                echo "<td style='border: 1px solid black;'>"
                     . $param .
                     "</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
}