<?php

namespace App\Animals\Cats;

use App\Animals\AnimalAbstract;

/**
 * class Cats
 */
class Cat extends AnimalAbstract
{
    /**
     * @var string
     */
    protected $breed;

    /**
     * @var string
     */
    protected $skinColor;

    /**
     * @var int
     */
    protected $age;

    /**
     * Cats constructor.
     * @param $breed
     * @param $skinColor
     * @param $age
     * @param $temperament
     */
    public function __construct($age, $breed, $skinColor = 'black',
                                $temperament = 'funny')
    {
        $this->breed = $breed;
        $this->skinColor = $skinColor;
        $this->age = $age;
        $this->temperament = $temperament;
    }

    /**
     * giving cat type
     * @return string
     */
    public function catType()
    {
        return $this->skinColor . " " . $this->breed;
    }

    /**
     * calculating cat's birth year
     * @param $currentYear
     * @return int
     */
    public function getYearBirth($currentYear)
    {
        return $currentYear - $this->age;
    }

    /**
     * figuring out what cat eats
     * @return string
     */
    public function whatItEats()
    {
        return "mice";
    }

    /**
     * get cat's voice
     * @return string
     */
    public function getVoice()
    {
        return "Meeeoowww";
    }

    /**
     * get cat's skill
     */
    public function getSkill()
    {
        return "clever";
    }

    public function getCatInfo()
    {
        return [
            "age"           =>  $this->age,
            "breed"         =>  $this->breed,
            "skin color"    =>  $this->skinColor,
            "temperament"   =>  $this->temperament
        ];
    }
}