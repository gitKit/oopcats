<?php

namespace App\Animals\Cats;

class HouseCat extends Cat
{
    /**
     * name of house cat
     * @var string
     */
    protected $name;


    /**
     * HouseCats constructor.
     * @param $name
     * @param $age
     * @param $breed
     * @param string $skinColor
     * @param string $temperament
     */
    public function __construct($name, $age, $breed, $skinColor = 'black', $temperament = 'funny')
    {
        $this->name = $name;
        parent::__construct($age, $breed, $skinColor, $temperament);
    }

    public function whatItEats()
    {
        return 'fish, sausages and sometimes ' . parent::whatItEats();
    }

    public function getSkill()
    {
        return "lazy lying";
    }

    /**
     * get cat's data
     * @return array
     */
    public function getCatInfo()
    {
        $resArray["name"] = $this->name;
        $resArray += parent::getCatInfo();

        return $resArray;
    }
}