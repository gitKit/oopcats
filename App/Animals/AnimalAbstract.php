<?php

namespace App\Animals;

use App\Interfaces\CharacterInterface;
use App\Interfaces\CreatureInterface;

abstract class AnimalAbstract implements CreatureInterface, CharacterInterface
{
    /**
     * @var string
     */
    protected $temperament;

    /**
     * figuring out its food
     * @return string
     */
    abstract public function whatItEats();

    /**
     * @inheritdoc
     */
    abstract public function getVoice();

    /**
     * @inheritdoc
     */
    abstract public function getSkill();

    /**
     * @inheritdoc
     */
    public function getTemperament()
    {
        return $this->temperament;
    }
}