<?php


namespace App\Service;


use Medoo\Medoo;

class Database
{
    private static $connection;

    private static $config = array(
        'database_type'     =>  'mysql',
        'database_name'     =>  'animals',
        'server'            =>  'localhost',
        'username'          =>  'root',
        'password'          =>  'rootroot'
    );

    public static function getConnection()
    {
        if (self::$connection == null)
        {
            self::$connection = new Medoo(self::$config);
        }

        return self::$connection;
    }

    public static function catsData()
    {
        $database = self::getConnection();

        $data = $database->select("cats",[
            "id",
            "age",
            "breed",
            "skin_color",
            "temperament",
        ]);

        $data = json_encode($data);
        $arr = json_decode($data, true);

        return $arr;
    }

    public static function houseCatsData()
    {
        $database = self::getConnection();

        $data = $database->select("house_cats",[
            "id",
            "name",
            "age",
            "breed",
            "skin_color",
            "temperament",
        ]);

        $data = json_encode($data);
        $arr = json_decode($data, true);

        return $arr;
    }

}