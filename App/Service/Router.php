<?php

namespace App\Service;


use App\Controller\Home;
use App\Controller\Cats;
use App\Controller\HouseCats;

use Klein\Klein;

class Router
{
    public static function dispatch()
    {
        $klein = new Klein();

        $klein->respond('GET', '/', function () {
            $controller = new Home();
            $controller->execute();
        });

        $klein->respond('GET', '/index.php/cat', function () {
            $controller = new Cats();
            $controller->execute();
        });

        $klein->respond('GET', '/index.php/house_cat', function () {
            $controller = new HouseCats();
            $controller->execute();
        });

        $klein->dispatch();
    }
}

